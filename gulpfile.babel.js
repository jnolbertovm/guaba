"use strict";

import {
    series,
    parallel
} from "gulp";

import {
    clean,
    cleanJS,
    cleanSCSS
} from "./gulp/task/clean.babel";
import {
    scssTask,
    scssWatch
} from "./gulp/task/scss.babel";
import {
    jsTask,
    jsWatch
} from "./gulp/task/js.babel";

global.notify = {
    ignoreInitial: true,
    event: 'change'
};

exports.clean = clean;
exports.build = series(clean, parallel(scssTask));
exports.js = series(cleanJS, jsTask, jsWatch);
exports.jsBuild = series(cleanJS, jsWatch);
exports.scss = series(cleanSCSS, scssTask, scssWatch);
exports.scssBuild = series(cleanSCSS, scssTask);
exports.default = series(
    clean,
    series(scssTask, jsTask),
    scssWatch,
    jsWatch
);