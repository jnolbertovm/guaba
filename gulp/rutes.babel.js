import main from './main.babel';

const SRC = global.src;
const DIST = global.dist;

module.exports = {
    scss: {
        src: SRC + 'scss/guaba.scss',
        dest: DIST + 'css/',
        watch: SRC + 'scss/**/*+(.sass|.scss)'
    },
    js: {
        src: SRC + 'js/**/main.js',
        dest: DIST + 'js/',
        watch: SRC + 'js/**/*.js'
    },
    clean: {
        js: DIST + 'js/**',
        scss: DIST + 'css/**',
        all: [
            DIST + 'css/**',
            DIST + 'js/**',
            DIST + 'img/**',
            DIST + 'fonts/**'
        ]
    }
};