"use strict";

import params from "./params.babel";
import noty from "gulp-notify";

var notify = () => {
    let pnoty = params.notify;

    if (global.notify.event === 'add') {
        pnoty.title = 'Se agregó un nuevo archivo';
    } else if (global.notify.event === 'unlink') {
        pnoty.title = 'Se elimino el archivo:';
    } else {
        pnoty.title = 'Se modificó el archivo:';
    }

    global.notify.ignoreInitial = false;

    return noty(pnoty);
}

export {
    notify
};