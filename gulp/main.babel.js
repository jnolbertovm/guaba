const BASE_DIR = require('path').resolve(__dirname, '../');
const SRC = './src/';
const DIST = './assets/';

global.div = '||';
global.src = SRC;
global.dist = DIST;

module.exports = {
  isProd: false,
  linter: true, //INSPECCIONA LA CORRECTA SINTAXIS DEL CODIGO A TODOS LOS SCRIPTS Y STYLES
  valid: true, //INSPECCIONA ERRORES EN EL CODIGO DE LOS ARCHIVOS SCRIPTS Y STYLES
  mapper: true, //CREA A TODOS LOS ARCHIVOS DE TIPO STYLE EL *.map,
  baseDir: BASE_DIR,
  handlerScriptFiles: [],
  handlerStyleFiles: [],
  extensions: {
    script: [".js"],
    style: [".css", ".less", ".scss"],
    image: [".jpg", ".jpeg", ".png", ".svg", ".ico"],
    font: [".eot", ".ttf", ".woff", ".woff2", ".otf", ".svg"]
  }
}
