import main from './main.babel';

module.exports = {
    sass: {
        outputStyle: main.isProd ? 'compressed' : 'expanded',
        includePaths: []
    },
    rename: {
        suffix: '.min'
    },
    autoprefixer: {
        supports: 'ie8',
        cascade: false,
        grid: true
    },
    sourcemaps: {
        loadMaps: true,
        largeFile: true
    },
    notify: {
        title: "Se modificó un archivo",
        message: "<%= file.relative %> "
    }
};