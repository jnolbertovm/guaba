import {
    src,
    dest
} from "gulp";
import main from "../main.babel";

const jsTask = (cb) => {
    console.log("js");
    cb();
}
const jsWatch = (cb) => {
    console.log("js");
    cb();
}

module.exports = {
    jsTask,
    jsWatch
};