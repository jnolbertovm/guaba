import {
    src,
    dest,
    watch
} from "gulp";
import main from "../main.babel";
import params from "../params.babel";
import rutes from "../rutes.babel";
import {
    notify
} from "../utils.babel";
import gulpif from "gulp-if";
import pumbler from "gulp-plumber";
import sass from "gulp-sass";
import rename from "gulp-rename";
import sourcemaps from "gulp-sourcemaps";
import autoprefixer from "gulp-autoprefixer";

const scssTask = (cb) => {
    return src(rutes.scss.src)
        .pipe(pumbler({
            errorHandler: (error) => {
                console.log(error);
            }
        }))
        .pipe(gulpif(main.mapper, sourcemaps.init(params.sourcemaps)))
        .pipe(sass(params.sass))
        .pipe(rename(params.rename))
        .pipe(autoprefixer(params.autoprefixer))
        .pipe(pumbler.stop())
        .pipe(dest(rutes.scss.dest))
        .pipe(gulpif(!global.notify.ignoreInitial, notify()))
        .pipe(gulpif(main.mapper, sourcemaps.write('.')))
        .pipe(gulpif(main.mapper, dest(rutes.scss.dest)));
};

const scssWatch = (cb) => {
    watch(rutes.scss.watch, scssTask)
        .on('change', function (path, stats) {
            global.notify.event = "change";
        })
        .on('add', function (path, stats) {
            global.notify.event = "add";
        })
        .on('unlink', function (path, stats) {
            global.notify.event = "unlink";
        });
};

module.exports = {
    scssTask,
    scssWatch
};