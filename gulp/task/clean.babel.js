import del from "del";
import rutes from "../rutes.babel";

const clean = (cb) => {
    del((!Array.isArray(rutes.clean.all)) ? [rutes.clean.all] : rutes.clean.all, cb);
    cb();
};

const cleanJS = (cb) => {
    del((!Array.isArray(rutes.clean.js)) ? [rutes.clean.js] : rutes.clean.js, cb);
    cb();
};

const cleanSCSS = (cb) => {
    del((!Array.isArray(rutes.clean.scss)) ? [rutes.clean.scss] : rutes.clean.scss, cb);
    cb();
};

module.exports = {
    clean,
    cleanJS,
    cleanSCSS
};